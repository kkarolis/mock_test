# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.
{
    'name': 'Test Mocking',
    'version': '0.1',
    'author': 'HacBee UAB',
    'category': 'Custom',
    'website': 'http://www.hbee.eu',
    'summary': '',
    'description': """
Short description about module
""",
    'depends': [
    ],
    'data': [
    ],
    'installable': True,
    'application': False,
}
